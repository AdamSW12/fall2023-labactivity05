package polymorphism;

public class BookStore
{
    public static void main( String[] args )
    {
        Book[] books = new Book[5];

        //Book objects
        books[0] = new Book("House Of Leaves","Mark Z. Danielewski");
        books[2] = new Book("Looking For Alaska", "John Green");

        //ElectronicBook objects
        books[1] = new ElectronicBook("Songs of a Dead Dreamer and Grimscribe","Thomas Ligotti",2100);
        books[3] = new ElectronicBook("The Strange Case of Dr. Jekyll and Mr. Hyde","Robert Louis Stevenson",1000);
        books[4] = new ElectronicBook("The Picture of Dorian Gray","Oscar Wilde",1300);

        for (Book book : books) {
            System.out.println(book);
        }

        // books[0].getNumberBytes();
        // books[1].getNumberBytes();

        ElectronicBook b = (ElectronicBook)books[1];

        System.out.println(b.getNumberBytes());

        Book a = (ElectronicBook)books[0];
        System.out.println(a.getNumberBytes());
    }
}
